package com.wipro.testcases;

import org.openqa.selenium.chrome.ChromeDriver;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.Checkbox;
import java.io.*;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import com.wipro.testbase.Testbase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC01 extends Testbase{
	BufferedWriter writer;
	//launch the application
	@BeforeClass
	public void start() throws IOException {
		writer = new BufferedWriter(new FileWriter("writer.txt", false));
        
		driver=new ChromeDriver();
        driver.get("https://demo.opencart.com");
	}
	//closing the application
	@AfterClass
	public void stop() throws IOException {
		writer.close();
		driver.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111748\\Assign3\\resources\\config\\config.properties"));
		String url = properties.getProperty("url");
		driver.get(url);
		System.out.println(url);

	}
	//Click on Registration
	@Test
	public void test2() throws InterruptedException {
		 driver.findElement(By.linkText("My Account")).click();
	     driver.findElement(By.linkText("Register")).click();
	     assertEquals("https://demo.opencart.com/index.php?route=account/register",driver.getCurrentUrl());
	}
	//Registration
	@Test
    public void test3() throws BiffException, IOException, InterruptedException {
          File f=new File("D:\\registraion.xls");
          Workbook b=Workbook.getWorkbook(f);
        Sheet s1=b.getSheet(0);
        int col=s1.getColumns();
        String []s=new String[col];
        for(int i=0;i<col;i++){
            Cell c=s1.getCell(i, 0);
            s[i]=c.getContents();
            System.out.println(s[i]);
        }
        driver.findElement(By.name("firstname")).sendKeys(s[0]);
        driver.findElement(By.name("lastname")).sendKeys(s[1]);
        driver.findElement(By.name("email")).sendKeys(s[2]);
        driver.findElement(By.name("telephone")).sendKeys(s[3]);
        driver.findElement(By.name("password")).sendKeys(s[4]);
        driver.findElement(By.name("confirm")).sendKeys(s[5]);
     }
	//verify the check box has been checked
	@Test
	public void test4() throws IOException, InterruptedException{
        driver.findElement(By.xpath("//input[@type='checkbox']")).click();
        
        writer.write("Checkbox is checked \n");
       assertTrue(driver.findElement(By.xpath("//input[@type='checkbox']")).isEnabled()); 
    }
	//Click on continue button
	@Test
	public void test5() throws IOException, InterruptedException{
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		TakesScreenshot scrShot =((TakesScreenshot)driver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        File output=new File("D:\\20111748\\Assign3\\resources\\screenshots\\output.png");
        FileUtils.copyFile(SrcFile, output);
		    writer.write("Congratulations! Your new account has been successfully created!\n");
		    assertEquals("https://demo.opencart.com/index.php?route=account/success",driver.getCurrentUrl());
	}
	//click on 'my account'
	@Test
	public void test6() throws IOException, InterruptedException{
		driver.findElement(By.linkText("My Account")).click();
	}
	//logout
	@Test
	public void test7() throws IOException, InterruptedException{
		driver.findElement(By.linkText("Logout")).click();
		writer.write("successfully logged out!");
		assertEquals("https://demo.opencart.com/index.php?route=account/logout",driver.getCurrentUrl());
	}
}


