package com.wipro.testcases;


	import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.*;
import java.util.List;
import java.util.Properties;

	import org.apache.commons.io.FileUtils;
	import org.openqa.selenium.By;
	import org.openqa.selenium.OutputType;
	import org.openqa.selenium.TakesScreenshot;
	import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
	import org.testng.annotations.*;

	import com.wipro.testbase.Testbase;
	import jxl.Cell;
	import jxl.Sheet;
	import jxl.Workbook;
	import jxl.read.biff.BiffException;
	public class TC04 extends Testbase{
		BufferedWriter wrt;
		//launch the application
		@BeforeClass
		public void start() throws IOException {
			wrt = new BufferedWriter(new FileWriter("wrt.txt", false));
			driver=new ChromeDriver();
	        driver.get("https://demo.opencart.com");
		}
		//close the driver
		@AfterClass
		public void stop() throws IOException {
			wrt.close();
			driver.close();
		}
		@Test
		public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
			Properties properties=new Properties();
			properties.load(new FileInputStream("D:\\20111748\\Assign3\\resources\\config\\config.properties"));
			String url = properties.getProperty("url");
			driver.get(url);
			System.out.println(url);
			Thread.sleep(2000);
		}
		//the user clicks on My Account and Login
		@Test
		public void test2() throws InterruptedException {
			driver.findElement(By.linkText("My Account")).click();
			 driver.findElement(By.linkText("Login")).click();
			
		}
		//Login
		@Test
	    public void test3() throws BiffException, IOException, InterruptedException {
	          File f=new File("D:\\login.xls");
	          Workbook b=Workbook.getWorkbook(f);
	        Sheet s2=b.getSheet(0);
	        int col=s2.getColumns();
	        String []s=new String[col];
	        for(int i=0;i<col;i++){
	            Cell c=s2.getCell(i, 0);
	            s[i]=c.getContents();
	            System.out.println(s[i]);
	        }
	        driver.findElement(By.name("email")).sendKeys(s[0]);
	        driver.findElement(By.name("password")).sendKeys(s[1]);
	       
	       
	        Thread.sleep(2000);
	     }
		//clicks the Login button
		@Test
		public void test4() throws IOException, InterruptedException{
		 driver.findElement(By.xpath("//input[@type='submit']")).click();
		 TakesScreenshot scrShot =((TakesScreenshot)driver);
	     File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	     File output6=new File("D:\\20111748\\Assign3\\resources\\screenshots\\output6.png");
	     FileUtils.copyFile(SrcFile, output6);
	     assertEquals("https://demo.opencart.com/index.php?route=account/account",driver.getCurrentUrl());
		}
		//counts the number of menu links
		@Test
		public void test5() throws IOException, InterruptedException{
			WebElement nav=driver.findElement(By.className("navbar-nav"));
			List<WebElement> navs=nav.findElements(By.tagName("a"));
			int cnt=0;
			for(WebElement ele:navs) {
				if(!ele.getText().isEmpty())
				{
					wrt.write(ele.getText()+"\n");
					cnt++;
				}
		}
			wrt.write("count="+cnt);
			System.out.print("count="+cnt);
		}
		//clicks on each menu link 1 by 1 and takes snapshot
		@Test
		public void test6() throws IOException, InterruptedException{
			driver.findElement(By.linkText("Desktops")).click();
			 TakesScreenshot scrShot =((TakesScreenshot)driver);
		     File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
		     File nl1=new File("D:\\20111748\\Assign3\\resources\\screenshots\\nl1.png");
		     FileUtils.copyFile(SrcFile, nl1);
		     
			driver.findElement(By.linkText("Laptops & Notebooks")).click();
			TakesScreenshot scrShot2 =((TakesScreenshot)driver);
		     File SrcFile2=scrShot2.getScreenshotAs(OutputType.FILE);
		     File nl2=new File("D:\\20111748\\Assign3\\resources\\screenshots\\nl2.png");
		     FileUtils.copyFile(SrcFile2, nl2);
		     
		     driver.findElement(By.linkText("Components")).click();
		     TakesScreenshot scrShot3 =((TakesScreenshot)driver);
		     File SrcFile3=scrShot3.getScreenshotAs(OutputType.FILE);
		     File nl3=new File("D:\\20111748\\Assign3\\resources\\screenshots\\nl3.png");
		     FileUtils.copyFile(SrcFile3, nl3);
		     
		     driver.findElement(By.linkText("Tablets")).click();
		     TakesScreenshot scrShot4 =((TakesScreenshot)driver);
		     File SrcFile4=scrShot4.getScreenshotAs(OutputType.FILE);
		     File nl4=new File("D:\\20111748\\Assign3\\resources\\screenshots\\nl4.png");
		     FileUtils.copyFile(SrcFile4, nl4);
		     
		     driver.findElement(By.linkText("Software")).click();
		     TakesScreenshot scrShot5 =((TakesScreenshot)driver);
		     File SrcFile5=scrShot5.getScreenshotAs(OutputType.FILE);
		     File nl5=new File("D:\\20111748\\Assign3\\resources\\screenshots\\nl5.png");
		     FileUtils.copyFile(SrcFile5, nl5);
		     
		     driver.findElement(By.linkText("Phones & PDAs")).click();
		     TakesScreenshot scrShot6 =((TakesScreenshot)driver);
		     File SrcFile6=scrShot6.getScreenshotAs(OutputType.FILE);
		     File nl6=new File("D:\\20111748\\Assign3\\resources\\screenshots\\nl6.png");
		     FileUtils.copyFile(SrcFile6, nl6);
		     
		     driver.findElement(By.linkText("Cameras")).click();
		     TakesScreenshot scrShot7 =((TakesScreenshot)driver);
		     File SrcFile7=scrShot7.getScreenshotAs(OutputType.FILE);
		     File nl7=new File("D:\\20111748\\Assign3\\resources\\screenshots\\nl7.png");
		     FileUtils.copyFile(SrcFile7, nl7);
		     
		     driver.findElement(By.linkText("MP3 Players")).click();
		     TakesScreenshot scrShot8 =((TakesScreenshot)driver);
		     File SrcFile8=scrShot8.getScreenshotAs(OutputType.FILE);
		     File nl8=new File("D:\\20111748\\Assign3\\resources\\screenshots\\nl8.png");
		     FileUtils.copyFile(SrcFile8, nl8);
		     
		}
		
		//click on 'my account'
		@Test
		public void test7() throws IOException, InterruptedException{
			driver.findElement(By.linkText("My Account")).click();
			}
		//logout
		@Test
		public void test8() throws IOException, InterruptedException{
			driver.findElement(By.linkText("Logout")).click();
			wrt.write("successfully logged out!");
			assertEquals("https://demo.opencart.com/index.php?route=account/logout",driver.getCurrentUrl());
		}
}
