package com.wipro.testcases;
import static org.testng.Assert.assertEquals;

import java.io.*;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import com.wipro.testbase.Testbase;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC02 extends Testbase {
	BufferedWriter writer;
	//launch the application
	@BeforeClass
	public void start() throws IOException {
		writer = new BufferedWriter(new FileWriter("writer.txt", false));
		driver=new ChromeDriver();
        driver.get("https://demo.opencart.com");
	}
	//Closing the driver
	@AfterClass
	public void stop() throws IOException {
		writer.close();
		driver.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111748\\Assign3\\resources\\config\\config.properties"));
		String url = properties.getProperty("url");
		driver.get(url);
		System.out.println(url);
		
	}
	//Click on the login button
	@Test
	public void test2() throws InterruptedException {
		driver.findElement(By.linkText("My Account")).click();
		 driver.findElement(By.linkText("Login")).click();
		
	}
	//Login to the account
	@Test
    public void test3() throws BiffException, IOException, InterruptedException {
          File f=new File("D:\\login.xls");
          Workbook b=Workbook.getWorkbook(f);
        Sheet s2=b.getSheet(0);
        int col=s2.getColumns();
        String []s=new String[col];
        for(int i=0;i<col;i++){
            Cell c=s2.getCell(i, 0);
            s[i]=c.getContents();
            System.out.println(s[i]);
        }
        driver.findElement(By.name("email")).sendKeys(s[0]);
        driver.findElement(By.name("password")).sendKeys(s[1]);  
        
     }
	//Click on Login button
	@Test
	public void test4() throws IOException, InterruptedException{
	 driver.findElement(By.xpath("//input[@type='submit']")).click();
	 TakesScreenshot scrShot =((TakesScreenshot)driver);
     File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
     File output2=new File("D:\\20111748\\Assign3\\resources\\screenshots\\output2.png");
     FileUtils.copyFile(SrcFile, output2);
     assertEquals("https://demo.opencart.com/index.php?route=account/account",driver.getCurrentUrl());
	}
	//Click on Edit account link
	@Test
	public void test5() throws IOException, InterruptedException{
		 driver.findElement(By.linkText("Edit your account information")).click();
		assertEquals("https://demo.opencart.com/index.php?route=account/edit",driver.getCurrentUrl());
	}
	//Edit new telephone number
	@Test
	public void test6() throws IOException, InterruptedException{
		 driver.findElement(By.name("telephone")).clear();
		 String[] str= {"34567"};
		 driver.findElement(By.name("telephone")).sendKeys(str);
	}
	//Click on the continue button
	@Test
	public void test7() throws IOException, InterruptedException{
		 driver.findElement(By.xpath("//input[@type='submit']")).click();
		 TakesScreenshot scrShot =((TakesScreenshot)driver);
	     File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	     File output3=new File("D:\\20111748\\Assign3\\resources\\screenshots\\output3.png");
	     FileUtils.copyFile(SrcFile, output3);
	     assertEquals("https://demo.opencart.com/index.php?route=account/account",driver.getCurrentUrl());
	    
	}
	//click on 'my account'
	@Test
	public void test8() throws IOException, InterruptedException{
		driver.findElement(By.linkText("My Account")).click();
		}
	//logout
	@Test
	public void test9() throws IOException, InterruptedException{
		driver.findElement(By.linkText("Logout")).click();
		writer.write("successfully logged out!");
		assertEquals("https://demo.opencart.com/index.php?route=account/logout",driver.getCurrentUrl());
	}
	}

