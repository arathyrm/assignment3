package com.wipro.testcases;

import static org.testng.Assert.assertEquals;

import java.io.*;
import java.util.List;
import java.util.Properties;

	import org.apache.commons.io.FileUtils;
	import org.openqa.selenium.By;
	import org.openqa.selenium.OutputType;
	import org.openqa.selenium.TakesScreenshot;
	import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
	import org.testng.annotations.*;

	import com.wipro.testbase.Testbase;
	import jxl.Cell;
	import jxl.Sheet;
	import jxl.Workbook;
	import jxl.read.biff.BiffException;
	public class TC05 extends Testbase{
		BufferedWriter wrt;
		//launch the application
		@BeforeClass
		public void start() throws IOException {
			wrt = new BufferedWriter(new FileWriter("wrt.txt", false));
			driver=new ChromeDriver();
	        driver.get("https://demo.opencart.com");
		}
		//closes the driver
		@AfterClass
		public void stop() throws IOException {
			wrt.close();
			driver.close();
		}

		@Test
		public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
			Properties properties=new Properties();
			properties.load(new FileInputStream("D:\\20111748\\Assign3\\resources\\config\\config.properties"));
			String url = properties.getProperty("url");
			driver.get(url);
			System.out.println(url);
			Thread.sleep(2000);
		}
		//the user clicks on My Account and Login
		@Test
		public void test2() throws InterruptedException {
			driver.findElement(By.linkText("My Account")).click();
			 driver.findElement(By.linkText("Login")).click();
			
		}
		//Login
		@Test
	    public void test3() throws BiffException, IOException, InterruptedException {
	          File f=new File("D:\\login.xls");
	          Workbook b=Workbook.getWorkbook(f);
	        Sheet s2=b.getSheet(0);
	        int col=s2.getColumns();
	        String []s=new String[col];
	        for(int i=0;i<col;i++){
	            Cell c=s2.getCell(i, 0);
	            s[i]=c.getContents();
	            System.out.println(s[i]);
	        }
	        driver.findElement(By.name("email")).sendKeys(s[0]);
	        driver.findElement(By.name("password")).sendKeys(s[1]);
	       
	       
	        Thread.sleep(2000);
	     }
		//clicks the Login button
		@Test
		public void test4() throws IOException, InterruptedException{
			 driver.findElement(By.xpath("//input[@type='submit']")).click();
		 TakesScreenshot scrShot =((TakesScreenshot)driver);
	     File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	     File output6=new File("D:\\20111748\\Assign3\\resources\\screenshots\\output6.png");
	     FileUtils.copyFile(SrcFile, output6);
	     assertEquals("https://demo.opencart.com/index.php?route=account/account",driver.getCurrentUrl());
		}
		//click on 'Your store'
		@Test
		public void test5() throws IOException, InterruptedException{
			driver.findElement(By.linkText("Your Store")).click();
			assertEquals("https://demo.opencart.com/index.php?route=common/home",driver.getCurrentUrl());
		}
		//clicks on Brands link available on bottom navbar under Extras
		@Test
		public void test6() throws IOException, InterruptedException{
			driver.findElement(By.linkText("Brands")).click();
			assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer",driver.getCurrentUrl());
		}
		//Click on all Brands links one by one 
		@Test
		public void test7() throws IOException, InterruptedException{
			 driver.findElement(By.linkText("Apple")).click();
			 assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer/info&manufacturer_id=8",driver.getCurrentUrl());
			 TakesScreenshot scrShota =((TakesScreenshot)driver);
		     File SrcFilea=scrShota.getScreenshotAs(OutputType.FILE);
		     File apple=new File("D:\\20111748\\Assign3\\resources\\screenshots\\apple.png");
		     FileUtils.copyFile(SrcFilea, apple);
		     
		     driver.findElement(By.linkText("Brands")).click();
		     driver.findElement(By.linkText("Canon")).click();
		     assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer/info&manufacturer_id=9",driver.getCurrentUrl());
			 TakesScreenshot scrShotc =((TakesScreenshot)driver);
		     File SrcFilec=scrShotc.getScreenshotAs(OutputType.FILE);
		     File canon=new File("D:\\20111748\\Assign3\\resources\\screenshots\\canon.png");
		     FileUtils.copyFile(SrcFilec, canon);
		     
		     driver.findElement(By.linkText("Brands")).click();
		     driver.findElement(By.linkText("Hewlett-Packard")).click();
		     assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer/info&manufacturer_id=7",driver.getCurrentUrl());
			 TakesScreenshot scrShothp =((TakesScreenshot)driver);
		     File SrcFilehp=scrShothp.getScreenshotAs(OutputType.FILE);
		     File hp=new File("D:\\20111748\\Assign3\\resources\\screenshots\\hp.png");
		     FileUtils.copyFile(SrcFilehp, hp);
		     
		     driver.findElement(By.linkText("Brands")).click();
		     driver.findElement(By.linkText("HTC")).click();
		     assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer/info&manufacturer_id=5",driver.getCurrentUrl());
			 TakesScreenshot scrShoth =((TakesScreenshot)driver);
		     File SrcFileh=scrShoth.getScreenshotAs(OutputType.FILE);
		     File htc=new File("D:\\20111748\\Assign3\\resources\\screenshots\\htc.png");
		     FileUtils.copyFile(SrcFileh, htc);
		     
		     driver.findElement(By.linkText("Brands")).click();
		     driver.findElement(By.linkText("Palm")).click();
		     assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer/info&manufacturer_id=6",driver.getCurrentUrl());
			 TakesScreenshot scrShotp =((TakesScreenshot)driver);
		     File SrcFilep=scrShotp.getScreenshotAs(OutputType.FILE);
		     File palm=new File("D:\\20111748\\Assign3\\resources\\screenshots\\palm.png");
		     FileUtils.copyFile(SrcFilep, palm);
		     
		     driver.findElement(By.linkText("Brands")).click();
		     driver.findElement(By.linkText("Sony")).click();
		     assertEquals("https://demo.opencart.com/index.php?route=product/manufacturer/info&manufacturer_id=10",driver.getCurrentUrl());
			 TakesScreenshot scrShots =((TakesScreenshot)driver);
		     File SrcFiles=scrShots.getScreenshotAs(OutputType.FILE);
		     File sony=new File("D:\\20111748\\Assign3\\resources\\screenshots\\sony.png");
		     FileUtils.copyFile(SrcFiles, sony);
		}
		//click on 'my account'
		@Test
		public void test8() throws IOException, InterruptedException{
			driver.findElement(By.linkText("My Account")).click();
		}
		//logout
		@Test
		public void test9() throws IOException, InterruptedException{
			driver.findElement(By.linkText("Logout")).click();
			wrt.write("successfully logged out!");
			assertEquals("https://demo.opencart.com/index.php?route=account/logout",driver.getCurrentUrl());
		}
}